from sanic import Sanic
from sanic import response
app = Sanic()

@app.route("/")
async def test(request):

    return response.text("Events")

if __name__ == "__main__":
    print('Web was started...')
    app.run(host="0.0.0.0", port=8903)