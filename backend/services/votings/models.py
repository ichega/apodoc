from peewee import *
#
db = PostgresqlDatabase('postgres', user='postgres', password='example',
                           host='postgres_db')
import datetime


# db = SqliteDatabase('gavno.db')

class User(Model):
    id = AutoField()
    first_name = CharField(max_length = 100)
    last_name = CharField(max_length=100)
    email = CharField(max_length=100)
    password = CharField(max_length=100)
    company = CharField(max_length=100)
    position = CharField(max_length=100)

    class Meta:
        database = db # This model uses the "people.db" database.


class Session(Model):
    id = AutoField()
    user_id = IntegerField()
    access_key = CharField(max_length=100)


    class Meta:
        database = db  # This model uses the "people.db" database.


class Voting(Model):
    id = AutoField()
    name = CharField(max_length=100)
    description = CharField(max_length=100, default="")
    start = DateTimeField(default=datetime.datetime.now(), null=True)
    finish = DateTimeField(default=datetime.datetime.now(), null=True)
    active = BooleanField(default=True)
    class Meta:
        database = db  # This model uses the "people.db" database.


class Question(Model):
    id = AutoField()
    text = CharField(max_length=100)
    voting = IntegerField()

    class Meta:
        database = db  # This model uses the "people.db" database.


class UserQuestion(Model):
    id = AutoField()
    answer = IntegerField()
    comment = CharField(max_length=100)
    user = IntegerField()
    question = IntegerField()

    class Meta:
        database = db  # This model uses the "people.db" database.


class Files(Model):
    id = AutoField()
    name = CharField(max_length=100)

    class Meta:
        database = db  # This model uses the "people.db" database.

class Atachments(Model):
    id = AutoField()
    file = IntegerField()
    message = IntegerField(default=-1)
    question = IntegerField(default=-1)

    class Meta:
        database = db


class UserVoting(Model):
    id = AutoField()
    voting = IntegerField()
    user = IntegerField()
    is_admin = BooleanField()

    class Meta:
        database = db

class UserVoting(Model):
    id = AutoField()
    voting = IntegerField()
    user = IntegerField()
    is_admin = BooleanField()

    class Meta:
        database = db

class Message(Model):
    id = AutoField()
    text = CharField()
    date = DateTimeField(default=datetime.datetime.now())
    user = IntegerField()
    voting = IntegerField()

    class Meta:
        database = db