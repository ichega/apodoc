from peewee import *

db = PostgresqlDatabase('postgres', user='postgres', password='example',
                           host='postgres_db')

# db = SqliteDatabase('gavno.db')

class User(Model):
    id = AutoField()
    first_name = CharField(max_length = 100)
    last_name = CharField(max_length=100)
    email = CharField(max_length=100)
    password = CharField(max_length=100)
    company = CharField(max_length=100)
    position = CharField(max_length=100)

    class Meta:
        database = db # This model uses the "people.db" database.


class Session(Model):
    id = AutoField()
    user_id = IntegerField()
    access_key = CharField(max_length=100)


    class Meta:
        database = db  # This model uses the "people.db" database.