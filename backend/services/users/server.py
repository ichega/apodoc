from sanic import Sanic
from sanic import response
from sanic.response import json

app = Sanic()
from models import *
import uuid
from playhouse.migrate import *
from peewee import *


# @app.route("/")
# async def test(request):
#     return response.text("Users")


@app.route("/users/register", methods=["POST"])
async def sign_up(request):
    user = User.get_or_none(User.email == request.args.get('email'))

    if user == None:
        user = User(
            email=request.args.get('email'),
            first_name=request.args.get('first_name'),
            last_name=request.args.get('last_name'),
            password=request.args.get("password"),
            company=request.args.get("company"),
            position=request.args.get("position")
        )
        user.save()
        return json({"code": "0"})
    return json({"code": "1"})


@app.route("/users/login", methods=["POST"])
async def sign_in(request):
    email = request.args.get('email')
    password = request.args.get("password")
    print(1)
    user = User.get_or_none(User.email == email)
    print(2)
    if user != None:
        print(3)
        if user.password == password:
            print(4)
            ak = str(uuid.uuid4())
            print(5)
            response = json({
                "code": "0",
                "data": {
                    "id": user.id,
                    "email": user.email,
                    "last_name": user.last_name,
                    "first_name": user.first_name,
                    "company": user.company,
                    "position": user.position,
                    "access_key": ak,
                }
            })
            print(6)
            response.cookies['access_key'] = ak
            print(7)
            print(ak)
            session = Session(user_id=int(user.id), access_key=ak)
            session.save()
            return response
        else:
            return json({"code": "1",
                         "error": "password"})
    else:
        return json({"code": "1",
                     "error": "email"})


@app.route("/users/logout", methods=["POST"])
async def sign_out(request):
    print(dict(request.cookies))
    if "access_key" in request.cookies:
        response = json({"ddd": "ddddd"})
        del response.cookies['access_key']
        return response


@app.route("/users/current", methods=["GET"])
async def users_current_view(request):
    if "access_key" in request.cookies:
        session = Session.get_or_none(Session.access_key == request.cookies["access_key"])
        if session != None:
            user = User.get_or_none(User.id == session.user_id)
            data = {
                "id": user,
                "email": user.email,
                "last_name": user.last_name,
                "first_name": user.first_name,
                "company": user.company,
                "position": user.position,
            }

            return response.json(data)


@app.route("/users/search/<find_str>", methods=["GET"])
async def users_find_view(request, find_str):
    if "access_key" in request.cookies:
        session = Session.get_or_none(Session.access_key == request.cookies["access_key"])
        if session != None:

            users = User.select()
            found = []
            for user in users:
                if (str(user.first_name).find(find_str) != -1) or \
                    (str(user.last_name).find(find_str) != -1) or\
                    (str(user.email).find(find_str) != -1) or\
                    (str(user.company).find(find_str) != -1) or\
                    (str(user.position).find(find_str) != -1):
                    found.append(user)



            return response.json({
                "count": len(found),
                "users": [user for user in found]
            })


@app.route("/users", methods=["GET"])
async def get_users(request):
    users = User.select()
    resp = []
    for user in users:
        resp.append({"email": user.email,
                     "last_name": user.last_name,
                     "first_name": user.first_name,
                     "password": user.password,
                     "id": user.id,
                     })
    return json({"users": resp})


@app.route("/users/<user_id>", methods=["GET"])
async def users_by_id(request, user_id):
    # if "access_key" in request.cookies:
    #     session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #     if session != None:
    user = User.get_or_none(User.id == user_id)
    if user != None:

        resp = {"email": user.email,
                "last_name": user.last_name,
                "first_name": user.first_name}
        return json({"user": resp})
    else:
        return json({"code": 1})


@app.route("/users_by_name", methods=["GET"])
async def users_by_name(request):
    # if "access_key" in request.cookies:
    #     session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #     if session != None:
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    user = User.get_or_none(User.first_name == first_name and User.last_name == last_name)
    if user != None:
        resp = {
            "id": user.id,
            "email": user.email,
            "last_name": user.last_name,
            "first_name": user.first_name}
    return json({"user": resp})


@app.route("/users_by_company", methods=["GET"])
async def get_users_by_company(request):
    # if "access_key" in request.cookies:
    #     session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #     if session != None:
    users = User.select().where(User.company == request.args.get('company'))
    resp = []
    for user in users:
        resp.append({"email": user.email,
                     "last_name": user.last_name,
                     "first_name": user.first_name})
    return json({"users": resp})
    #     else:
    #         return json({"code": "1"})
    # else:
    #     return json({"code": "1"})


@app.route("/test_request_args")
async def test_request_args(request):
    print(request.args.get('test'))
    return json({"ddd": "ddddd"})


@app.route("/database/drop")
async def database_drop_view(request):
    db.drop_tables([User, Session])
    return json({})


@app.route("/database/init")
async def database_init_view(request):
    print(1111)
    try:
        db.connect()
        User.create_table()
        Session.create_table()
    except peewee.InternalError as px:
        print(str(px))
    # db.create_tables([User, Session])

    return json({})


@app.route("/migrations")
async def migrations(request):
    my_db = PostgresqlDatabase('postgres', user='postgres', password='example',
                               host='postgres_db')
    migrator = PostgresqlMigrator(my_db)

    d = IntegerField()
    migrate(
        migrator.drop_column('session', 'user'),
        migrator.add_column('session', 'user_id', d),
    )


if __name__ == "__main__":
    print('Web was started...')
    app.run(host="0.0.0.0", port=8905)
