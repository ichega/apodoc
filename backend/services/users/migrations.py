from playhouse.migrate import *
from peewee import *


my_db = PostgresqlDatabase('postgres', user='postgres', password='example',
                           host='postgres_db')
migrator = PostgresqlMigrator(my_db)

d = IntegerField()
migrate(
    migrator.drop_column('Session', 'user'),
    migrator.add_column('Session', 'user_id', d),
)