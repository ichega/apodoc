from sanic import Sanic
from sanic import response
from jinja2 import Environment, FileSystemLoader
app = Sanic()
app.static('/static', './static')

env = Environment(loader=FileSystemLoader('./templates'))

async def test(request, path=None):
    template = env.get_template('index.html')
    context = {

    }
    return response.html(template.render(context))

app.add_route(test, '/')
app.add_route(test, '/<path>')

if __name__ == "__main__":
    print('Web was started...')
    app.run(host="0.0.0.0", port=8901)