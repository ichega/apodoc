from sanic import Sanic
from sanic import response, request
import aiohttp
import sanic_graphql
from sanic_graphql import GraphQLView
import graphene
import collections
from models import *

app = Sanic()

class VotingGQL(graphene.ObjectType):
    id = graphene.String()
    name = graphene.String()
    description = graphene.String()
    percent = graphene.String()

class Query(graphene.ObjectType):
    votings = graphene.List(VotingGQL)

    def resolve_votings(self, info):
        user_id = 1
        uv = UserVoting.select().where(UserVoting.user == user_id)
        res = []
        for u in uv:
            if u.is_admin == True:
                voting = Voting.get_or_none(Voting.id == u.voting)

                percent = 0
                for question in Question.select().where(Question.voting == voting.id):
                    percent += UserQuestion.select().where(UserQuestion.question == question.id).count()
                percent = (percent / (UserVoting.select().where(UserVoting.voting == voting.id).count()) * (
                    Question.select().where(Question.voting == voting.id).count())) * 100

                res.append({"name": voting.name,
                            "description": voting.description,
                            "percent": str(percent),
                            "id": voting.id
                            })
        return res




# USERS_PORT =

users_list = [
    {
        "id": "1",
        "name": "admin",
        "email": "admin1@admin.admin"
    },
    {
        "id": "2",
        "name": "admin2",
        "email": "admin2@admin.admin"
    },
    {
        "id": "3",
        "name": "admin3",
        "email": "admin3@admin.admin"
    }
]





schema = graphene.Schema(query=Query)


# query = """
# query something{
# users {
#     id
#     name
#     email
# }
# }
# """
# res = schema.execute(query)
# print(dict(res.data["users"][0]))

app.add_route(GraphQLView.as_view(schema=schema, graphql=True), '/graphql')


@app.route("/users")
async def users_view(request):
    async with aiohttp.ClientSession() as session:
        async with session.get('http://users:8905/users') as resp:
            data = await resp.json()

    return response.json(data)

@app.route("/users/login", methods=["POST"])
async def users_login_view(request):
    async with aiohttp.ClientSession() as session:
        async with session.post(f'http://users:8905/users/login?{request.query_string}') as resp:
            data = await resp.json()
            print(data)



    resp = response.json(data)
    if "data" in data:
        resp.cookies["access_key"] = data["data"]["access_key"]
    return resp

@app.route("/users/current", methods=["GET"])
async def users_current_view(request):
    async with aiohttp.ClientSession(cookies=request.cookies) as session:
        async with session.get(f'http://users:8905/users/current') as resp:
            print(await resp.text())
            data = await resp.json()
            print(data)



    resp = response.json(data)
    return resp

@app.route("/users/search/<find_str>", methods=["GET"])
async def users_find_view(request, find_str):
    async with aiohttp.ClientSession(cookies=request.cookies) as session:
        async with session.get(f'http://users:8905'+request.path) as resp:
            data = await resp.json()
            print(data)

    resp = response.json(data)
    return resp

@app.route("/votings", methods=["POST"])
async def create_new_votings_view(request):
    async with aiohttp.ClientSession(cookies=request.cookies) as session:
        async with session.post(f'http://votings:8906/voting', json=request.json, headers={"Content-Type": "application/json"}) as resp:
            print('LOL'+await resp.text())

            data = await resp.json()

    resp = response.json(data)
    return resp

@app.route("/users/register", methods=["POST"])
async def users_login_view(request):
    async with aiohttp.ClientSession() as session:
        async with session.post(f'http://users:8905/users/register?{request.query_string}') as resp:
            data = await resp.json()
    resp = response.json(data)
    return resp

@app.route("/users/logout", methods=["POST"])
async def users_logout_view(request):
    async with aiohttp.ClientSession(cookies=request.cookies) as session:
        async with session.post(f'http://users:8905/users/logout') as resp:
            print(await resp.text())
            data = await resp.json()
            cookies = resp.cookies
    resp = response.json(data)
    del resp.cookies["access_key"]



    return resp


@app.route("/")
async def web_view1(request):
    async with aiohttp.ClientSession() as session:
        async with session.get(f'http://web:8901{request.path}') as resp:
            data = await resp.text()

    return response.html(data)

@app.route("/<path>")
async def web_view2(request, path=None):
    async with aiohttp.ClientSession() as session:
        async with session.get(f'http://web:8901{request.path}') as resp:
            data = await resp.text()

    return response.html(data)



@app.route("/database/init")
async def database_init_view(request):
    print(1111)
    try:
        db.connect()
        User.create_table()
        Session.create_table()
        Voting.create_table()
        Question.create_table()
        UserQuestion.create_table()
        Files.create_table()
        Atachments.create_table()
        UserVoting.create_table()
        UserVoting.create_table()
        Message.create_table()
    except peewee.InternalError as px:
        print(str(px))


@app.route("/voting",methods=["POST"])
async def set_votings(request):
    # if "access_key" in request.cookies:
    #      session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #      if session != None:
    #          user_id = session.user_id
    user_id = 1
    data = request.json
    print(data)
    owner_id = data["owner_id"]["id"]
    voting = Voting(
        name=request.json["name"],
        description = request.json["description"],
        active=bool(request.json["active"]),
    )
    voting.save()



    user_voting = UserVoting(
        voting = voting.id,
        user=owner_id,
        is_admin=True
    )
    user_voting.save()

    questions = [
        Question(text=q["value"], voting=voting.id)
        for q in request.json["questions"]
    ]
    for q in questions:
        q.save()

    return response.json({"id":voting.id})


@app.route("/question",methods=["POST"])
async def add_question(request):
    # if "access_key" in request.cookies:
    #      session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #      if session != None:
    #          user_id = session.user_id

    data = request.json
    user_id = data["user_id"]
    voting_id = data["voting"]
    uv = UserVoting.get_or_none(UserVoting.user == int(user_id))
    if uv.voting == int(voting_id):
        if uv.is_admin == True:
            question = Question(
                text = data["text"],
                voting = voting_id
            )
            question.save()
        return response.json({"code":"0"})




@app.route("/voting",methods=["GET"])
async def get_votings(request):
    if "access_key" in request.cookies:
         session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
         if session != None:
             user_id = session.user_id

             voting = Voting.get_or_none(Voting.name == request.args.get('name'))
             if voting != None:
                 return response.json({"id":voting.id})
             else:
                 return response.json({"code": "1"})

@app.route("/voting/<voting_id>", methods=["GET"])
async def get_voting_info(request, voting_id):
    # if "access_key" in request.cookies:
    #     session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #     if session != None:
    data = request.json
    user1 = data["user_id"]
    voting = Voting.get_or_none(Voting.id == voting_id)
    if voting != None:
        messages = Message.select().where(Message.voting == voting.id)
        res_mes = []
        for message in messages:
            user = User.get(User.id == message.user)
            res_mes.append({
                "date":str(message.date),
                "text": str(message.text),
                "sender": user.first_name + " "  + user.last_name,
                "is_mine": str(user==user1)
            })

        res_ques = []
        for question in Question.select().where(Question.voting == voting.id):
            za = 0
            protiv = 0
            vozd = 0
            for us_ques in UserQuestion.select().where(UserQuestion.question == question.id):
                if us_ques.answer == 1:
                    za += 1
                elif us_ques.answer == 2:
                    protiv += 1
                else:
                    vozd += 1
            res_ques.append({"text":question.text,
                             "za": str(za),
                             "protiv": str(protiv),
                             "vozd": str(vozd)})

        return response.json({"name": voting.name,
                     "messages":res_mes,
                     "questions":res_ques})
    else:
        return response.json({"code": 1})


@app.route("/vote", methods=["POST"])
async def vote(request):
    data = request.json
    ques = Question.get_or_none(Question.id == data["question"])
    voting = Voting.get_or_none(Voting.id == ques.voting)


    uv = UserVoting.get_or_none(UserVoting.user == data["user_id"])
    if uv != None:
        uq = UserQuestion.get_or_none(UserQuestion.user == data["user_id"])
        if uq.question == data["question"]:
            if uq == None:
                uq = UserQuestion(
                    answer = int(data["answer"]),
                    comment = data["comment"],
                    user = data["user_id"],
                    question = data["question"],
                )
            else:
                uq.answer = int(data["answer"])
                uq.comment = data["comment"]
                uq.user = data["user_id"]
                uq.question = data["question"]
            uq.save()
            return response.json({"code":"0"})


@app.route("/message", methods=["POST"])
async def add_message(request):
    # if "access_key" in request.cookies:
    #     session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #     if session != None:


    data = request.json
    # user = User.get(id=session.user_id)
    user = User.get_or_none(User.id == data["user_id"])
    text = data["text"]
    voting = data["voting"]
    message = Message(text = text,
                      date = datetime.datetime.now(),
                      user = user.id,
                      voting = voting)
    message.save()
    return response.json({"id": message.id})
    # except:
    #     return json({"code": "hz"})

@app.route("/get_voting_for_user", methods=["GET"])
async def get_voting_for_user(request):
    user_id = request.args.get('user_id')
    uv = UserVoting.select().where(UserVoting.user == user_id)
    res = []

    for u in uv:
        if u.is_admin == False:
            voting = Voting.get_or_none(Voting.id == u.voting)
            uv2 = UserVoting.get_or_none(UserVoting.voting == voting.id & UserVoting.is_admin == True)
            admin = User.get_or_none(User.id == uv2.user)
            percent = 0
            for question in Question.select().where(Question.voting == voting.id):
                percent += UserQuestion.select().where(UserQuestion.question == question.id).count()
            percent = (percent/(UserVoting.select().where(UserVoting.voting == voting.id).count())*(Question.select().where(Question.voting==voting.id).count()))*100

            res.append({"name":voting.name,
                        "description":voting.description,
                        "admin": admin.first_name + " " + admin.last_name,
                        "percent": str(percent),
                        "id": voting.id})
    return response.json({"res":res})

@app.route("/get_voting_for_admin", methods=["GET"])
async def get_voting_for_admin(request):
    user_id = request.args.get('user_id')
    uv = UserVoting.select().where(UserVoting.user == user_id)
    res = []
    for u in uv:
        if u.is_admin == True:
            voting = Voting.get_or_none(Voting.id == u.voting)

            percent = 0
            for question in Question.select().where(Question.voting == voting.id):
                percent += UserQuestion.select().where(UserQuestion.question == question.id).count()
            percent = (percent / (UserVoting.select().where(UserVoting.voting == voting.id).count()) * (
                Question.select().where(Question.voting == voting.id).count())) * 100

            res.append({"name": voting.name,
                        "description": voting.description,
                        "percent": str(percent),
                        "id": voting.id
                        })
    return response.json({"res":res})

@app.route("/user_voting", methods=["POST"])
async def add_user_to_voting(request):
    # if "access_key" in request.cookies:
    #      session = Session.get_or_none(Session.access_key == request.cookies['access_key'])
    #      if session != None:
    #          user_id = session.user_id
    user_id = 1
    data = request.json
    voting = Voting.get_or_none(Voting.id == data['voting'])
    uv = UserVoting.get_or_none(UserVoting.user == user_id and UserVoting.voting == voting.id)
    if uv.is_admin == True:
        uv2 = UserVoting(
            voting=voting.id,
            user= data["user_id"],
            is_admin=False
        )
        uv2.save()
        return response.json({"code": 0})
    else:
        return response.json({"code": 1})


# app.static('/static/', '../web/static/')

# @app.route("/static/<path:path>")
# async def web_static_view(request, path=None):
#     async with aiohttp.ClientSession() as session:
#         url = f'http://web:8901{request.path}'
#         print(url)
#         async with session.get(url) as resp:
#             data = await resp.text()
#
#     return response.text(data)

if __name__ == "__main__":
    print('Web was started...')
    app.run(host="0.0.0.0", port=8904)
