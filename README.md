# Apodoc

## Очень простой способ все забилдить и запустить:
```shell script
bash deploy.sh
```

## Команды для сборки и запуска:


Сборка:
1. Frontend
```shell script
webpack --mode development

```
2. Backend
```shell script
docker-compose build
```

Запуск:
```shell script
docker-compose up
```

Локальный запуск
```shell script
source venv/bin/activate
pythno manage.py runserver 127.0.0.1:8901
```

## Порты для микросервисов

- web: 8901
- events: 8903
- gateway: 8904
- users: 8905
- votings: 8906
- db: 15432

**URL для фронта**: `http://apodoc.ru`

### Подключение к PostgreSQL

login: `postgres`

password: `example`

host: `127.0.0.1`

port: `15432`