const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
module.exports = {
    module: {
        rules: [
            // ... other rules
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'url-loader'
            }

        ]
    },
    entry: './frontend/index.js',
    output: {
        filename: './backend/services/web/static/js/bundle.js',
        path: path.resolve(__dirname)
    },
    plugins: [
        // make sure to include the plugin!
        new VueLoaderPlugin()
    ]
};