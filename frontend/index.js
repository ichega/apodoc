//Imports Apps or Libs
import Vue from 'vue';
import Vuex from 'vuex';


import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import VueRouter from 'vue-router';

import locale from 'element-ui/lib/locale/lang/ru-RU.js'

Vue.use(ElementUI, {locale});
Vue.use(VueRouter);


//Creating a Vue Instance
import App from './components/App.vue'
let app = new Vue(App).$mount('#app');

//Test log
console.log('Its work!!!');